package Creational_Pattern;

public abstract class DifficultyMenu implements MenuType{
  
  @Override
  public void menu_type() {
    System.out.println("1) Simples\r\n"
        + "2) Not-so-simples\r\n"
        + "3) Super-duper-shuffled");
  }
}

