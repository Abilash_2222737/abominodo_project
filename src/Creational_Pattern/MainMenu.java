package Creational_Pattern;

public abstract class MainMenu implements MenuType {
  @Override
  public void menu_type() {
    System.out.println("1) Play\r\n"
        + "2) View high scores\r\n"
        + "3) View rules\r\n"
        + "0) Quit");
  }
}

