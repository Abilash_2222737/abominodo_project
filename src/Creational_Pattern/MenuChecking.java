package Creational_Pattern;

public abstract class MenuChecking implements MenuType{
  MenuType menuType;
  
  public MenuChecking (MenuType menuType) {
    this.menuType=menuType;
  }
  
  @Override
  public void menu_type() {
    menuType.menu_type();
  }
}

