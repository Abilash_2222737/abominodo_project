package Creational_Pattern;

public class Menu {

  public static void main(String[] args) {
    
    MenuChecking menuChecking = new MenuChecking(new MainMenu() {}) {};
    menuChecking.menu_type();

    System.out.println("\n");
    
    menuChecking = new MenuChecking(new DifficultyMenu() {}) {};
    menuChecking.menu_type();
    
    System.out.println("\n");
    
    menuChecking = new MenuChecking(new PlayMenu() {}) {};
    menuChecking.menu_type();
  }

}

