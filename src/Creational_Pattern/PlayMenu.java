package Creational_Pattern;

public abstract class PlayMenu implements MenuType {
  @Override
  public void menu_type() {
    System.out.println("1) Print the grid\r\n"
        + "2) Print the box\r\n"
        + "3) Print the dominos\r\n"
        + "4) Place a domino\r\n"
        + "5) Unplace a domino\r\n"
        + "6) Get some assistance\r\n"
        + "7) Check your score\r\n"
        + "0) Given up");
  }
}

