package ObserverPattern;

public class messageMain {

  public static void main(String[] args) {
    Player01 player01=new Player01();
    Player02 player02=new Player02();
    Player03 player03=new Player03();
    
    MessagePublisher p = new MessagePublisher();
    
    p.attach(player01);
     
    p.notifyUpdate(new Message("Silver"));   
     
    p.detach(player01);
    p.attach(player02);
    p.attach(player03);
     
    p.notifyUpdate(new Message("Gold")); 
  }

}



