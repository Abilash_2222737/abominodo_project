package ObserverPattern;

public class Player03 implements Observer {
  @Override
  public void update(Message m) {
      System.out.println("Rank of Player 03 :: " + m.getMessageContent());
  }
}


