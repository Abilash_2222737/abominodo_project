package ObserverPattern;

public class Player02 implements Observer {
  @Override
  public void update(Message m) {
      System.out.println("Rank of Player 02 :: " + m.getMessageContent());
  }
}

