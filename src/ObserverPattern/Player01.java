package ObserverPattern;

public class Player01 implements Observer {
  @Override
  public void update(Message m) {
      System.out.println("Rank of Player 01 :: " + m.getMessageContent());
  }
}

