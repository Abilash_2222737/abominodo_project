package MVC_Pattern;

import java.awt.BorderLayout;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class MenuView {

//View uses Swing framework to display UI to user
  private JFrame frame;
  private JLabel firstnameLabel;
  private JLabel lastnameLabel;
  private JTextField firstnameTextfield;
  private JTextField lastnameTextfield;
  private JButton firstnameSaveButton;
  private JButton lastnameSaveButton;
  private JButton hello;
  private JButton bye;

  public MenuView(String title) {
    frame = new JFrame(title);
    frame.getContentPane().setLayout(new BorderLayout());
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setSize(500, 120);
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);
    // Create UI elements
    firstnameLabel = new JLabel("Firstname :");

    firstnameTextfield = new JTextField();

    firstnameSaveButton = new JButton("Save Firstname and Enter the Game");

    hello = new JButton("Hello!");
    bye = new JButton("Bye!");
    // Add UI element to frame
    GroupLayout layout = new GroupLayout(frame.getContentPane());
    layout.setAutoCreateGaps(true);
    layout.setAutoCreateContainerGaps(true);
    layout.setHorizontalGroup(layout.createSequentialGroup()
        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(firstnameLabel))
        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(firstnameTextfield))
        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(firstnameSaveButton))
        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(hello).addComponent(bye)));
    layout.setVerticalGroup(layout.createSequentialGroup()
        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(firstnameLabel)
            .addComponent(firstnameTextfield).addComponent(firstnameSaveButton).addComponent(hello))
        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(bye)));
    layout.linkSize(SwingConstants.HORIZONTAL, firstnameSaveButton);
    layout.linkSize(SwingConstants.HORIZONTAL, hello, bye);
    frame.getContentPane().setLayout(layout);
  }

  public JFrame getFrame() {
    return frame;
  }

  public void setFrame(JFrame frame) {
    this.frame = frame;
  }

  public JLabel getFirstnameLabel() {
    return firstnameLabel;
  }

  public void setFirstnameLabel(JLabel firstnameLabel) {
    this.firstnameLabel = firstnameLabel;
  }

  public JTextField getFirstnameTextfield() {
    return firstnameTextfield;
  }

  public void setFirstnameTextfield(JTextField firstnameTextfield) {
    this.firstnameTextfield = firstnameTextfield;
  }

  public JButton getFirstnameSaveButton() {
    return firstnameSaveButton;
  }

  public void setFirstnameSaveButton(JButton firstnameSaveButton) {
    this.firstnameSaveButton = firstnameSaveButton;
  }

  public JButton getHello() {
    return hello;
  }

  public void setHello(JButton hello) {
    this.hello = hello;
  }

  public JButton getBye() {
    return bye;
  }

  public void setBye(JButton bye) {
    this.bye = bye;
  }
}
