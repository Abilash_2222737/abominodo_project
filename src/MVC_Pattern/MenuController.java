package MVC_Pattern;
import javax.swing.JOptionPane;


public class MenuController {

  private MenuModel model;
  private MenuView view;
  public MenuController(MenuModel m, MenuView v) {
   model = m;
   view = v;
   initView();
  }
  public void initView() {
   view.getFirstnameTextfield().setText(model.getFirstname());

  }
  public void initController() {
   view.getFirstnameSaveButton().addActionListener(e -> saveFirstname());
   view.getHello().addActionListener(e -> sayHello());
   view.getBye().addActionListener(e -> sayBye());
  }
  private void saveFirstname() {
   model.setFirstname(view.getFirstnameTextfield().getText());
   JOptionPane.showMessageDialog(null, "Firstname saved : " + model.getFirstname(), "Info", JOptionPane.INFORMATION_MESSAGE);
  }
  private void sayHello() {
   JOptionPane.showMessageDialog(null, "Welcome To Abominodo - The Best Dominoes Puzzle Game in the Universe " 
  + model.getFirstname(), "Info " , JOptionPane.INFORMATION_MESSAGE);
  }
  private void sayBye() {
   System.exit(0);
  }
}

