package MVC_Pattern;

public class MenuMain {

  public static void main(String[] args) {
    MenuModel m = new MenuModel("Tony");
    MenuView v = new MenuView("Welcome to the game");
    MenuController c = new MenuController(m, v);
    c.initController();

  }

}
