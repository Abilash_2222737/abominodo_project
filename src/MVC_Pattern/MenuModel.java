package MVC_Pattern;

public class MenuModel {

  private String firstname;

  
  public MenuModel(String firstname) {
   this.firstname = firstname;
  }
  
  public String getFirstname() {
   return firstname;
  }
  
  public void setFirstname(String firstname) {
   this.firstname = firstname;
  }
  


}


